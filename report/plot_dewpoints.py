import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as mdates
import ast
from cycler import cycler

f = open('dewpoints.html')
colors = ['blue', 'magenta', 'orange', 'green', 'cyan']
line = f.readline()
while line != '' and not line.strip().startswith('[['):
    line = f.readline()
fig = plt.figure()
ax = fig.add_subplot(111)
ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
order = range(len(colors))
ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))
number = 0
while line != '' and line.strip().startswith('[['):
    temps = ast.literal_eval(line.strip())
    color = colors[number % len(colors)]
    dates = [mdates.date2num(datetime.strptime(t[0], '%Y-%m-%d %H:%M:%S')) for t in temps]
    ax.scatter(dates, [t[1] for t in temps], color=color)
    number += 1
    line = f.readline()
    print 'plotting line number', number
names = []


fig.autofmt_xdate(rotation=45)
fig.tight_layout()
plt.ylabel('$^\circ$C', fontsize=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.savefig('dewpoints.pdf')