#usr/bin/env zsh

# python doc_report.py
echo Do not forget to execute
echo     python doc_report.py
gs -dBATCH -dNOPAUSE -q -dPDFSETTINGS=/printer -sDEVICE=pdfwrite -sOutputFile=report.pdf \
    Sniffer_dew_points__all_but_PP1_and_external_.pdf \
    Sniffer_dew_points__PP1_.pdf \
    Sniffer_dew_points__external_.pdf \
    PP1_dew_points.pdf \
    Cooling_bundle_dew_points.pdf \
    Cooling_bundle_surface_temperatures_positive_end.pdf \
    Cooling_bundle_surface_temperatures_negative_end.pdf \
    CO2_volume_dew_points.pdf \
    CO2_volume_temperatures.pdf \
    Pneumatic_line_monitoring__Cooling_Pneumatic_line_dew_points_.pdf \
    Cooling_plant_humidities.pdf \
    Cooling_plant_leak_rates.pdf

