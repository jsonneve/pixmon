#!/usr/bin/env zsh

if [[ -a $1 ]]
    then
        echo there is $1
        startdate=$1
    else
        echo Usage:
        echo ./curlwbmpage.sh startdate enddate cookiefile
        startdate='2017.07.10'
        startdate=$1
fi

if [[ -a $2 ]]
    then
        echo there is $2
        enddate=$2
    else
        echo Usage:
        echo ./curlwbmpage.sh startdate enddate cookiefile
        enddate='2017.08.10'
        enddate=$2
fi

if [[ -a $3 ]]
    then
        echo there is $3
        cookiefile=$3
    else
        echo Usage:
        echo ./curlwbmpage.sh startdate enddate cookiefile
        cookiefile='cookiefiledemo.txt'
        cookiefile=$3
fi

if [[ -a $cookiefile ]]
then
    cookie=$(cat $cookiefile)
else
    echo ''
    echo '!!!!!!!!!!!'
    echo no cookiefile $cookiefile found
    echo '!!!!!!!!!!!'
    echo ''
    cookie='empty'
fi

echo 'startdate:' $startdate
echo 'enddate:' $enddate
echo 'cookiefile:' $cookiefile
echo ''
echo 'You entered:'
echo ./curlwbmpage.sh $startdate $enddate $cookiefile
echo 'Example use:'
echo ./curlwbmpage.sh 2017.08.04 2017.08.18 cookiefiledemo.txt



echo curl -o wbm.html -k \
    'https://cmswbm.cern.ch/cmsdb/servlet/ConditionBrowser?DISPLAY=1&SUBMIT=1D&OPTION=vs_time&STEP=3&BEGIN='${startdate}'_12:00:00&END='${enddate}'_12:00:00&PRESCALER=1&MINIMUM=0&MAXIMUM=8&cms_omds_lb.CMS_TRK_DCS_PVSS_COND.DRYGAS.Pressure__2=1&' \
    -H 'Host: cmswbm.cern.ch' \
    -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0'  \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'  \
    -H 'Accept-Language: en-US,en;q=0.5' --compressed  \
    -H 'Cookie: '${cookie} \
    -H 'DNT: 1'  \
    -H 'Connection: keep-alive'  \
    -H 'Upgrade-Insecure-Requests: 1'