import os
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as mdates
import ast
from cycler import cycler
import sys
import argparse
import numpy as np
#from matplotlib import colors as mcolors
#from matplotlib._color_data import BASE_COLORS, CSS4_COLORS, XKCD_COLORS









if __name__ == "__main__":


    # Gather arguments
    parser = argparse.ArgumentParser(description='Get all tracker plots.')
    parser.add_argument('--no-wget', '-n', dest='donotwget', action='store_true', help='Do not wget the html and textfiles for the plots again.')
    args = parser.parse_args()
    wget = not args.donotwget


    # Define colors
    #colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
    # Sort colors by hue, saturation, value and name.
    #by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name)
                            #for name, color in colors.items())
    #sorted_names = [name for hsv, name in by_hsv]
    #colors = sorted_names
    #colors = XKCD_COLORS
    d = {'5000cca229d10d09': {374851: 1}, '5000cca229cf3f8f': {372496:3},'5000cca229d106f9': {372496: 3, 372455: 2}, '5000cca229d0b3e4': {380904: 2, 380905: 1, 380906: 1, 386569: 1}, '5000cca229d098f8': {379296: 2, 379297: 2, 379299: 2, 379303: 1, 379306: 1, 379469: 1, 379471: 1, 379459: 1, 379476: 1, 379456: 4, 379609: 4}, '5000cca229d03957': {380160: 3, 380736: 3, 380162: 1, 380174: 1, 381072: 2, 379608: 2, 380568: 3, 380569: 1, 380570: 1, 379296: 3, 379300: 1, 380328: 3, 379306: 1, 380331: 1, 379824: 2, 379825: 1, 379827: 1, 380344: 1, 379836: 1, 379456: 3, 380737: 1, 380739: 1, 379462: 1, 379476: 1, 379992: 3, 379609: 1, 379994: 1, 379611: 1, 379621: 1, 380006: 1, 380904: 3, 380905: 1, 380907: 1, 380535: 3, 380536: 1, 380538: 1}, '5000cca229cf6d0b': {372768: 10, 372550: 15, 372616: 14, 372617: 20, 372653: 3, 372505: 2}, '5000cca229cec4f1': {372510: 132}}
    jet = plt.cm.jet
    colors = jet(np.linspace(0, 1, len(d)))

    # Define all urls and their plot names
    quantities = {}
    quantities['Sniffer dew points (all but PP1 and external)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d;enddate=last14d;nhours=14;timediv=d;p_title=Sniffer%20dew%20points%20%28all%20but%20PP1%20and%20external%29;height=600;width=1200;legend=on;location=ne;snf=215143;snf=215132;snf=215128;snf=215142;snf=215141;snf=215127;snf=215140;snf=215135;snf=215131;snf=215134;snf=215145;snf=215133;snf=215139;snf=215146;snf=215117;snf=215130;snf=215136;snf=215144;snf=215138;snf=215129;min=-65;max=-15;rmin=;rmax=;avgdg=1;prescale=1;.cgifields=RH;.cgifields=snfcorr;.cgifields=T;.cgifields=DP;.cgifields=legend;.cgifields=columns;.cgifields=MAR;.cgifields=todpp'
    quantities['Sniffer dew points (PP1)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Sniffer+dew+points+PP1&height=600&width=1200&legend=on&location=ne&snf=215149&snf=215148&snf=215137&snf=215151&min=-65&max=-15&rmin=&rmax=&avgdg=1&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['Sniffer dew points (external)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Sniffer+dew+points+external&height=600&width=1200&legend=on&location=ne&snf=215150&snf=215147&min=&max=&rmin=&rmax=&avgdg=1&prescale=5&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['PP1 dew points'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=PP1+dew+points&height=600&width=1600&legend=on&location=ne&columns=off&sensors=N-PP1-C-02-1z-ARD_800F5501&sensors=N-PP1-C-02-2z-ARD_CCEF2F01&sensors=N-PP1-C-03-1z-ARD_771F3001&sensors=N-PP1-C-03-2z-ARD_C9CA3601&sensors=N-PP1-C-04-1z-ARD_79013001&sensors=N-PP1-C-04-2z-ARD_96283001&sensors=N-PP1-C-05-1z-ARD_95283001&sensors=N-PP1-C-05-2z-ARD_361F3001&sensors=N-PP1-C-06-1z-ARD_BAE52F01&sensors=N-PP1-C-06-2z-ARD_D6F22F01&sensors=N-PP1-C-07-1z-ARD_18203001&sensors=N-PP1-C-07-2z-ARD_8E1F3001&sensors=N-PP1-C-08-1z-ARD_0D243001&sensors=N-PP1-C-08-2z-ARD_CD013001&sensors=N-PP1-C-09-1z-ARD_5A2B3001&sensors=N-PP1-C-09-2z-ARD_28285501&sensors=N-PP1-C-11-1z-ARD_73864101&sensors=N-PP1-C-11-2z-ARD_F0253001&sensors=N-PP1-C-12-1z-ARD_62083001&sensors=N-PP1-C-12-2z-ARD_1A2F3001&sensors=N-PP1-C-13-1z-ARD_98E82F01&sensors=N-PP1-C-13-2z-ARD_42F42F01&sensors=N-PP1-C-14-1z-ARD_DC954101&sensors=N-PP1-C-14-2z-ARD_0CF92F01&sensors=N-PP1-C-15-1z-ARD_B1ED2F01&sensors=N-PP1-C-15-2z-ARD_BC093001&sensors=N-PP1-C-16-1z-ARD_67E63601&sensors=N-PP1-C-16-2z-ARD_6E083001&sensors=N-PP1-C-17-1z-ARD_C1CA3601&sensors=N-PP1-C-17-2z-ARD_17303001&sensors=N-PP1-C-18-1z-ARD_28293001&sensors=N-PP1-C-18-2z-ARD_5B0A3001&sensors=P-PP1-A-02-1z-ARD_E4DD3601&sensors=P-PP1-A-02-2z-ARD_E6F32F01&sensors=P-PP1-A-03-1z-ARD_C72E3001&sensors=P-PP1-A-03-2z-ARD_B5093001&sensors=P-PP1-A-04-1z-ARD_DCE52F01&sensors=P-PP1-A-04-2z-ARD_D9F22F01&sensors=P-PP1-A-05-1z-ARD_31D62F01&sensors=P-PP1-A-05-2z-ARD_5EE93601&sensors=P-PP1-A-06-1z-ARD_69175501&sensors=P-PP1-A-06-2z-ARD_35F02F01&sensors=P-PP1-A-07-1z-ARD_65075501&sensors=P-PP1-A-07-2z-ARD_70285501&sensors=P-PP1-A-08-1z-ARD_620A3001&sensors=P-PP1-A-08-2z-ARD_C2BE3601&sensors=P-PP1-A-09-1z-ARD_A8EE2F01&sensors=P-PP1-A-09-2z-ARD_C6DB4101&sensors=P-PP1-A-11-1z-ARD_652A3001&sensors=P-PP1-A-11-2z-ARD_C1073001&sensors=P-PP1-A-12-1z-ARD_FBEE2F01&sensors=P-PP1-A-12-2z-ARD_ABDB4101&sensors=P-PP1-A-13-1z-ARD_20243001&sensors=P-PP1-A-13-2z-ARD_04F42F01&sensors=P-PP1-A-14-1z-ARD_E2EE2F01&sensors=P-PP1-A-14-2z-ARD_16F92F01&sensors=P-PP1-A-15-1z-ARD_981F3001&sensors=P-PP1-A-15-2z-ARD_4A243001&sensors=P-PP1-A-16-1z-ARD_24115501&sensors=P-PP1-A-16-2z-ARD_6FDF3601&sensors=P-PP1-A-17-1z-ARD_741F3001&sensors=P-PP1-A-17-2z-ARD_68233001&sensors=P-PP1-A-18-1z-ARD_1DCB3601&sensors=P-PP1-A-18-2z-ARD_102F3001&DP=on&min=&max=&rmin=&rmax=&avgdg=30&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['Cooling bundle dew points'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Cooling+Bundle+Dew+points&height=600&width=1200&legend=on&location=ne&sensors=N-CB-R-10-1z-ARD_8B093001&sensors=N-CB-R-10-2z-ARD_CC954101&sensors=N-CB-R-11-1z-ARD_19E23601&sensors=N-CB-R-11-2z-ARD_AED63601&sensors=N-CB-R-2-1z-ARD_D6EF2F01&sensors=N-CB-R-2-2z-ARD_5AC43601&sensors=N-CB-R-6-1z-ARD_53093001&sensors=N-CB-R-6-2z-ARD_47283001&sensors=N-CB-R-7-1z-ARD_A5C83601&sensors=N-CB-R-7-2z-ARD_E5E73601&sensors=P-CB-R-1-1z-ARD_1B283001&sensors=P-CB-R-1-2z-ARD_81864101&sensors=P-CB-R-12-1z-ARD_CFFF2F01&sensors=P-CB-R-12-2z-ARD_9CD23601&sensors=P-CB-R-5-1z-ARD_A6864101&sensors=P-CB-R-5-2z-ARD_791F3001&sensors=P-CB-R-8-1z-ARD_6F013001&sensors=P-CB-R-8-2z-ARD_7FD73601&sensors=P-CB-R-9-1z-ARD_E82E3001&sensors=P-CB-R-9-2z-ARD_E8E23601&DP=on&min=&max=&rmin=&rmax=&avgdg=10&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['Cooling bundle surface temperatures positive end'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Cooling+Bundle+Surface+Temperatures+%28Positive+End%29&height=600&width=1600&legend=on&location=ne&columns=off&sensors=P-CBSUR-MS12FNG-17-1z-ARD_04CCF103&sensors=P-CBSUR-MS12FNG-17-2z-ARD_E4F93D04&sensors=P-CBSUR-MS12FNG-18-1z-ARD_C96B6204&sensors=P-CBSUR-MS12FNG-18-2z-ARD_D8172603&sensors=P-CBSUR-MS12RAD-X-2z-ARD_4D1A6204&sensors=P-CBSUR-MS12SPL-17-1z-ARD_8D2C3E04&sensors=P-CBSUR-MS12SPL-18-1z-ARD_104A6204&sensors=P-CBSUR-MS1FNG-2-1z-ARD_FE726204&sensors=P-CBSUR-MS1FNG-2-2z-ARD_1D236204&sensors=P-CBSUR-MS1FNG-3-1z-ARD_1DEC6104&sensors=P-CBSUR-MS1FNG-3-2z-ARD_8A906204&sensors=P-CBSUR-MS1RAD-X-2z-ARD_54A26204&sensors=P-CBSUR-MS1SPL-2-1z-ARD_C82A6204&sensors=P-CBSUR-MS1SPL-3-1z-ARD_50C16204&sensors=P-CBSUR-MS5FNG-4-1z-ARD_842C3E04&sensors=P-CBSUR-MS5FNG-4-2z-ARD_86186204&sensors=P-CBSUR-MS5FNG-5-1z-ARD_52203E04&sensors=P-CBSUR-MS5FNG-5-2z-ARD_911C3E04&sensors=P-CBSUR-MS5FNG-6-1z-ARD_9B8AF103&sensors=P-CBSUR-MS5FNG-6-2z-ARD_5144E102&sensors=P-CBSUR-MS5FNG-7-1z-ARD_47203E04&sensors=P-CBSUR-MS5FNG-7-2z-ARD_BD102603&sensors=P-CBSUR-MS5RAD-X-2z-ARD_05DA6204&sensors=P-CBSUR-MS5SPL-4-1z-ARD_9BA2F103&sensors=P-CBSUR-MS5SPL-5-1z-ARD_A8013E04&sensors=P-CBSUR-MS5SPL-6-1z-ARD_681D3E04&sensors=P-CBSUR-MS5SPL-7-1z-ARD_B9266204&sensors=P-CBSUR-MS8FNG-11-1z-ARD_5BF1DA04&sensors=P-CBSUR-MS8FNG-11-2z-ARD_563CD904&sensors=P-CBSUR-MS8FNG-12-1z-ARD_4BEC3D04&sensors=P-CBSUR-MS8FNG-12-2z-ARD_354EDA04&sensors=P-CBSUR-MS8FNG-8-1z-ARD_1CDFD904&sensors=P-CBSUR-MS8FNG-8-2z-ARD_73C7D904&sensors=P-CBSUR-MS8FNG-9-1z-ARD_95ABD904&sensors=P-CBSUR-MS8FNG-9-2z-ARD_C900DA04&sensors=P-CBSUR-MS8RAD-X-2z-ARD_676CDA04&sensors=P-CBSUR-MS8SPL-11-1z-ARD_8190D904&sensors=P-CBSUR-MS8SPL-12-1z-ARD_354EDA04&sensors=P-CBSUR-MS8SPL-8-1z-ARD_0CB2D904&sensors=P-CBSUR-MS8SPL-9-1z-ARD_FA51DA04&sensors=P-CBSUR-MS9FNG-13-1z-ARD_872BDA04&sensors=P-CBSUR-MS9FNG-13-2z-ARD_455AD904&sensors=P-CBSUR-MS9FNG-14-1z-ARD_D15FDA04&sensors=P-CBSUR-MS9FNG-14-2z-ARD_58A36204&sensors=P-CBSUR-MS9FNG-15-1z-ARD_D91D6204&sensors=P-CBSUR-MS9FNG-15-2z-ARD_CC0C3E04&sensors=P-CBSUR-MS9FNG-16-1z-ARD_4669DA04&sensors=P-CBSUR-MS9FNG-16-2z-ARD_2CE83D04&sensors=P-CBSUR-MS9RAD-X-2z-ARD_29C16204&sensors=P-CBSUR-MS9SPL-13-1z-ARD_CF812403&sensors=P-CBSUR-MS9SPL-14-1z-ARD_2AF9D904&sensors=P-CBSUR-MS9SPL-15-1z-ARD_926ADA04&sensors=P-CBSUR-MS9SPL-16-1z-ARD_FD4DDA04&T=on&min=&max=&rmin=&rmax=&avgdg=30&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['Cooling bundle surface temperatures negative end'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Cooling+Bundle+Surface+Temperatures+%28Negative+End%29&height=600&width=1600&legend=on&location=ne&columns=off&sensors=N-CBSUR-MS10FNG-13-1z-ARD_E41C6204&sensors=N-CBSUR-MS10FNG-13-2z-ARD_5E6E6204&sensors=N-CBSUR-MS10FNG-14-1z-ARD_602B6204&sensors=N-CBSUR-MS10FNG-14-2z-ARD_00AF6204&sensors=N-CBSUR-MS10FNG-15-1z-ARD_F27D6204&sensors=N-CBSUR-MS10FNG-15-2z-ARD_E1316204&sensors=N-CBSUR-MS10RAD-X-2z-ARD_DE746204&sensors=N-CBSUR-MS10SPL-13-1z-ARD_41FE6204&sensors=N-CBSUR-MS10SPL-14-1z-ARD_62706204&sensors=N-CBSUR-MS10SPL-15-1z-ARD_0BA66204&sensors=N-CBSUR-MS11FNG-16-1z-ARD_1C066304&sensors=N-CBSUR-MS11FNG-16-2z-ARD_CFD96204&sensors=N-CBSUR-MS11FNG-17-1z-ARD_9DEB6204&sensors=N-CBSUR-MS11FNG-17-2z-ARD_24386204&sensors=N-CBSUR-MS11FNG-18-1z-ARD_74E16204&sensors=N-CBSUR-MS11FNG-18-2z-ARD_CE656204&sensors=N-CBSUR-MS11RAD-X-2z-ARD_EFA06204&sensors=N-CBSUR-MS11SPL-16-1z-ARD_3A346204&sensors=N-CBSUR-MS11SPL-17-1z-ARD_75CC6204&sensors=N-CBSUR-MS11SPL-18-1z-ARD_E41F6204&sensors=N-CBSUR-MS2FNG-2-1z-ARD_00796204&sensors=N-CBSUR-MS2FNG-2-2z-ARD_5BC56204&sensors=N-CBSUR-MS2FNG-3-1z-ARD_E48EF103&sensors=N-CBSUR-MS2FNG-3-2z-ARD_52FFD904&sensors=N-CBSUR-MS2RAD-X-2z-ARD_DC1A6204&sensors=N-CBSUR-MS2SPL-2-1z-ARD_6B506204&sensors=N-CBSUR-MS2SPL-3-1z-ARD_97116204&sensors=N-CBSUR-MS6FNG-4-1z-ARD_EEA16204&sensors=N-CBSUR-MS6FNG-4-2z-ARD_3F1E3E04&sensors=N-CBSUR-MS6FNG-5-1z-ARD_FC69B604&sensors=N-CBSUR-MS6FNG-5-2z-ARD_47906C04&sensors=N-CBSUR-MS6FNG-6-1z-ARD_B2AE8804&sensors=N-CBSUR-MS6FNG-6-2z-ARD_409B6D04&sensors=N-CBSUR-MS6FNG-7-1z-ARD_90353E04&sensors=N-CBSUR-MS6FNG-7-2z-ARD_99266204&sensors=N-CBSUR-MS6FNG-8-1z-ARD_E5976204&sensors=N-CBSUR-MS6FNG-8-2z-ARD_6DC66204&sensors=N-CBSUR-MS6FNG-9-1z-ARD_F4E26204&sensors=N-CBSUR-MS6FNG-9-2z-ARD_CF866204&sensors=N-CBSUR-MS6RAD-X-2z-ARD_7AE06204&sensors=N-CBSUR-MS6SPL-4-1z-ARD_E8D16204&sensors=N-CBSUR-MS6SPL-5-1z-ARD_8ED56204&sensors=N-CBSUR-MS6SPL-6-1z-ARD_4FAC6204&sensors=N-CBSUR-MS6SPL-7-1z-ARD_DE1F6204&sensors=N-CBSUR-MS6SPL-8-1z-ARD_64286204&sensors=N-CBSUR-MS6SPL-9-1z-ARD_5AC9F103&sensors=N-CBSUR-MS7FNG-11-1z-ARD_D79B6204&sensors=N-CBSUR-MS7FNG-11-2z-ARD_215AD904&sensors=N-CBSUR-MS7FNG-12-1z-ARD_30B06204&sensors=N-CBSUR-MS7FNG-12-2z-ARD_F8728704&sensors=N-CBSUR-MS7RAD-X-2z-ARD_25996204&sensors=N-CBSUR-MS7SPL-11-1z-ARD_55396204&sensors=N-CBSUR-MS7SPL-12-1z-ARD_96026204&T=on&min=&max=&rmin=&rmax=&avgdg=30&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['CO2 volume dew points'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=CO2+volume+dew+points&height=600&width=1200&legend=on&location=ne&sensors=N-CO2-X-14B15A-1z-ARD_DBBE3601&sensors=N-CO2-X-5B6A-1z-ARD_16037C01&sensors=N-EBHBCO2-6-15A-1z-ARD_43F97B01&sensors=N-PP1CO2-A-15-1z-ARD_5F0E7C01&sensors=N-PP1CO2-A-6-1z-ARD_6E177C01&sensors=P-CO2-X-14A15B-1z-ARD_1E115501&sensors=P-CO2-X-5A6B-1z-ARD_EBF82F01&sensors=P-EBHBCO2-6-14A-1z-ARD_C1E92F01&sensors=P-PP1CO2-C-14-1z-ARD_BAE92F01&sensors=P-PP1CO2-C-5-1z-ARD_2E115501&DP=on&min=&max=&rmin=&rmax=&avgdg=1&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['CO2 volume temperatures'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d;enddate=last14d;nhours=14;timediv=d;p_title=CO2%20volume%20temperatures;height=600;width=1200;legend=on;location=ne;sensors=N-CO2-X-14B15A-1z-ARD_DBBE3601;sensors=N-CO2-X-5B6A-1z-ARD_16037C01;sensors=N-EBHBCO2-6-15A-1z-ARD_43F97B01;sensors=N-PP1CO2-A-15-1z-ARD_5F0E7C01;sensors=N-PP1CO2-A-6-1z-ARD_6E177C01;sensors=P-CO2-X-14A15B-1z-ARD_1E115501;sensors=P-CO2-X-5A6B-1z-ARD_EBF82F01;sensors=P-EBHBCO2-6-14A-1z-ARD_C1E92F01;sensors=P-PP1CO2-C-14-1z-ARD_BAE92F01;sensors=P-PP1CO2-C-5-1z-ARD_2E115501;T=on;min=;max=;rmin=;rmax=;avgdg=1;prescale=1;.cgifields=RH;.cgifields=snfcorr;.cgifields=T;.cgifields=DP;.cgifields=legend;.cgifields=columns;.cgifields=MAR;.cgifields=todpp'
    quantities['Pneumatic line monitoring (Cooling Pneumatic line dew points)'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=7&timediv=d&p_title=Pneumatic+Line+Monitoring&height=600&width=1200&legend=on&location=ne&sensors=X-SS2USC-LINE80-X-1z-ARD_&sensors=X-TSCABINET-LINE-X-1z-ARD_&DP=on&plants=pneum_dewpoint&todpp=off&min=&max=&rmin=&rmax=&avgdg=20&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['Cooling plant humidities'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last14d&enddate=last14d&nhours=14&timediv=d&p_title=Cooling+plant+humidities&height=600&width=1200&legend=on&location=ne&plants=cms_trk_dcs_4%3ACaV%2FPixPlant.Actual.Measurements.param10&plants=cms_trk_dcs_4%3ACaV%2FSs1Plant.Actual.Measurements.param10&plants=cms_trk_dcs_4%3ACaV%2FSs2Plant.Actual.Measurements.param10&min=&max=&rmin=&rmax=&avgdg=1&prescale=1&.submit=Submit&.cgifields=snfcorr&.cgifields=RH&.cgifields=DP&.cgifields=T&.cgifields=MAR&.cgifields=columns&.cgifields=legend&.cgifields=todpp'
    quantities['Cooling plant leak rates'] = 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/tkPlot.pl?startdate=last24h;enddate=last24h;nhours=24;timediv=h;p_title=;height=600;width=1200;legend=on;location=ne;plants=cms_trk_dcs_4%3ACaV%2FSs1Plant.Actual.Measurements.param17;plants=cms_trk_dcs_4%3ACaV%2FSs2Plant.Actual.Measurements.param17;min=;max=;rmin=;rmax=;avgdg=1;prescale=1;.cgifields=RH;.cgifields=snfcorr;.cgifields=T;.cgifields=DP;.cgifields=legend;.cgifields=columns;.cgifields=MAR;.cgifields=todpp'


    for sniffers in quantities:
        # Make one plot for each url


        # Make a nice name that can be used to save to a file
        sniffername = sniffers.replace(' ', '_').replace('(', '_').replace(')', '_')

        # Create a wget command:
        dewpointcommand = "wget '"
        dewpointcommand += quantities[sniffers]
        html = sniffername + ".html" 
        dewpointcommand += "' -O " + html

        # Only get html file if it is not there:
        if not os.path.exists(html) and wget:
            print dewpointcommand
            out = os.popen(dewpointcommand).readlines()
            print 'got new ' + html
            print out
        print "Looking at html file", html


        # Read html file until one hits the raw data part:
        f = open(html, 'r')
        line = f.readline()
        while line != '' and not line.strip().startswith('<h3>Raw Data:</h3>'):
            line = f.readline()
        txtfiles = line.split('</a>')

        # Create a plot
        fig = plt.figure()
        ax = plt.subplot(111)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
        # colors = ['c', 'm', 'y', 'k', 'orange', 'g', 'b', 'gray']
        # order = range(len(colors))
        # ax.set_prop_cycle(cycler('color', colors) + cycler('lw', order))

        # To cycle through colors:
        number = 0

        # Read html line with textfile names, split into individual files with labels:
        for txtfile in txtfiles:
            if '=' not in txtfile:
                continue
            # Ignore the epoch files:
            if 'epoch' in txtfile: continue

            # Filename for in wget query (includes ? part):
            filename = txtfile[txtfile.index('=') + 1: txtfile.index('">') + 1].strip('"')

            # Labelname to put in legend:
            plotname = txtfile[txtfile.index('">') + 2:]

            # Filename to save textfile to:
            plotfilename = plotname.replace('+', 'plus').replace('-', 'minus').replace(' ', '_').replace('(', '_').replace(')', '_')


            print "filename", filename
            print "plotname", plotname
            print "txtfilename", plotfilename + '.txt'
            print ''


            # Create wget command, get if not present/necessary:
            wgetcommand = "wget 'https://test-trkwf.web.cern.ch/test-trkwf/cgi-bin/" + filename + "' -O " + plotfilename + ".txt"
            if not os.path.exists(plotfilename + '.txt') and wget:
                print wgetcommand
                dowget = os.popen(wgetcommand).readlines()
                print dowget
                print 'got new textfile', plotfilename

            # Extract values from file (take care of timestamp, which is date and time split by a space):
            temps = [l.strip().split() for l in open(plotfilename + '.txt').readlines()]
            temps = [[t[0] + ' ' + t[1], t[2]] for t in temps]

            # Assign a color:
            color = colors[number % len(colors)]

            # Convert timestamp string to datetime object, accounting for with and without seconds:
            if temps[0][0].count(':') == 2:
                dates = [mdates.date2num(datetime.strptime(t[0], '%Y-%m-%d %H:%M:%S')) for t in temps]
            else:
                dates = [mdates.date2num(datetime.strptime(t[0], '%Y-%m-%d %H:%M')) for t in temps]

            # Plot values:
            ax.scatter(dates, [t[1] for t in temps], color=color, label=plotname)

            # Keep track of amount of quantities in plot:
            number += 1
            line = f.readline()
            print 'txt file number', number


        # Shrink current axis by 20% to make space for legend:
        # box = ax.get_position()
        # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis, making more columns if necessary:
        handles, labels = ax.get_legend_handles_labels() 
        if len(labels) > 20:
            lgd = ax.legend(loc='center left', ncol=2, bbox_to_anchor=(1, 0.5), fontsize=14)
        elif len(labels) > 40:
            lgd = ax.legend(loc='center left', ncol=3, bbox_to_anchor=(1, 0.5), fontsize=14)
        else:
            lgd = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize=14)

        # Make grid lines on plot:
        ax.grid(True)

        # Set title:
        plt.title(sniffers)

        # Rotate dates
        fig.autofmt_xdate(rotation=45)

        # Make tight layout:
        fig.tight_layout()

        # Apply ylabel (degrees Celsius or relative humidity):
        plt.ylabel('$^\circ$C', fontsize=20)
        if 'humid' in sniffers.lower():
            plt.ylabel('relative humidity (%)')

        # Increase xtick size:
        plt.xticks(fontsize=16)
        plt.yticks(fontsize=16)

        # Limit plot to filled space (no empty space):
        mindate = min(dates)
        maxdate = max(dates)
        plt.xlim(mindate, maxdate)

        # Save as pdf:
        plt.savefig(sniffername + '.pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
        print "Saved to file", sniffername + '.pdf'
        # Save as png:
        plt.savefig(sniffername + '.png', bbox_extra_artists=(lgd,), bbox_inches='tight')
        print "Saved to file", sniffername + '.png'
        print ""
        print ""
