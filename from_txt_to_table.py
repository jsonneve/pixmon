import sql3 as sql
import glob

filename = 'txt/5102_EndCap.txt'
line = 'PixelEndCap_BpI_D2_ROG4/channel000   4.59299993515   2016-07-18 08:13:51.143000'


for filename in glob.glob('txt/*.txt')[:]:
    if filename.count('_') > 1: continue
    print 'filename', filename
    if 'fillinfo' in filename.lower(): continue
    with open(filename, 'r') as ofi:
        line = ofi.readline()
        if line == '': continue

        #print 'line', line
        values = {}
        values['fill'] = int(filename[filename.rindex('/') + 1:filename.rindex('_')])
        values['name'] = line.split()[0]
        values['value'] = line.split()[1]
        values['time'] = line[line.index(line.split()[2]):].strip()
        values['detector'] = values['name'].split('_')[0]
        values['halfshell'] = values['name'].split('_')[1]
        if not 'air' in values['name'].split('_')[2].lower():
            values['disksector'] = values['name'].split('_')[2]
            values['layerreadoutgroup'] = values['name'].split('_')[3].split('/')[0]
            values['channel'] = values['name'].split('_')[3].split('/')[1]
        else:
            values['channel'] = 'air'
            values['disksector'] = None
            values['layerreadoutgroup'] = None
        values['layer'] = values['layerrreadoutgroup'].strip('LAY').strip('ROG')
        if values['layerreadoutgroup'] == 'LAY1':
            # Phase 0 layer 1 + 2:
            if values['channel'] == 'channel002':
                values['layer'] = 1
                values['qty'] = 'hv'
            elif values['channel'] == 'channel003':
                values['layer'] = 2
                values['qty'] = 'hv'
        elif values['layerrreadoutgroup'] == 'LAY3':
            # Phase 0 layer 3:
            if values['channel'] == 'channel002':
                values['layer'] = 3
                values['qty'] = 'hvch1'
            elif values['channel'] == 'channel003':
                values['layer'] = 3
                values['qty'] = 'hvch2'
        elif values['layerrreadoutgroup'] == 'LAY14':
            #Phase 1 layers 1 + 4:
            if values['channel'] == 'channel002':
                values['layer'] = 1
                values['qty'] = 'hv'
            elif values['channel'] == 'channel003':
                values['layer'] = 4
                values['qty'] = 'hv'
        elif values['layerrreadoutgroup'] == 'LAY23':
            # Phase 1 layers 2+3:
            if values['channel'] == 'channel002':
                values['layer'] = 2
                values['qty'] = 'hv'
            elif values['channel'] == 'channel003':
                values['layer'] = 3
                values['qty'] = 'hv'
        if values['channel'] == 'channel000':
            values['qty'] = 'digital'
        if values['channel'] == 'channel001':
            values['qty'] = 'analog'


        #for val in values:
        #    print val, values[val]
        sql.dict_to_db(values, 'pixeldb.db', 'currentstemperatures')
        print 'create table currentstemperatures(' + ''.join([val + ' text,' for val in values]) + ' unique(' + ''.join(['val' + ' ' for val in values]) + ') on conflict replace)'
        break
