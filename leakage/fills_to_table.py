#!/usr/bin/env zsh
import sql3 as sql
import sys

db = "currents.db"
tb = "currents__vs_lumi"
tbfills = "fills"
if __name__ == "__main__":
    if len(sys.argv) > 1:
        names = open(sys.argv[1], 'r')
    else:
        names = open('fills.txt', 'r')
    line = names.readline()
    headers_currents = ['detector', 'sector', 'layerrog', 'quantity', 'current', 'change_date', 'alias', 'channel', 'detpart']
    headers_fills = ['fill', 'begin', 'end', 'intlumi']
    headers = ['detector', 'sector', 'layerrog', 'quantity', 'current', 'changedate', 'fill', 'intlumi']
    print 'create table currents_vs_lumi (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
    print 'create table fills (' + ''.join([h + ' text, ' for h in headers_fills]) + 'unique(' + ''.join([h + ', ' for h in headers_fills]) + ' ) on conflict replace);'
    stop = 0
    maxlines = 3000
    maxlines = 13
    line = names.readline()
    channels = {'channel000': 'dig', 'channel001': 'ana', 'channel002': 'HV1', 'channel003': 'HV2'}
    while line != '': #and stop < maxlines:
        #vals = line.split(',')
        vals = line.split()
        values = {}
        values['fill'] = vals[0]
        values['begin'] =  '"' + vals[1]
        values['begin'] += ' ' + vals[2] + '"'
        values['end'] =  '"' + vals[3]
        values['end'] += ' ' + vals[4] + '"'
        values['intlumi'] = vals[5]

        stop += 1
        sql.dict_to_db(values, db, tbfills)
        line = names.readline()
