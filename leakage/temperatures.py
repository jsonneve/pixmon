from matplotlib import pyplot as plt
import numpy as np

temps = 'temperatures_perposition.txt'



if __name__ == "__main__":


    ############################
    ######    not used #########
    ############################
    # get temperatures from file
    temperatures = {}
    with open(temps) as tempsread:
        for line in tempsread:
            name, temp, notneeded = line.split()
            if temp == 'null':
                temp = np.nan
            else:
                temp = float(temp)
            temperatures[name] = temp

    ############################


    # make up some values for each x,y some z value (temperature):
    xyz = [(1, 1, 1), (1, 2, 3), (1, 3, 1.5), (1, 4, 2.33)]
    xyz.extend([(2, 1, 1), (2, 2, 3), (2, 3, 1.5), (2, 4, 2.33)])
    xyz.extend([(3, 1, 1), (3, 2, 2), (3, 3, np.nan), (3, 4, 2.33)])
    xyz.extend([(4, 1, 1), (4, 2, np.nan), (4, 3, 1.23), (4, 4, 1.5)])

    # get x, y, and z separately
    x, y, z = zip(*xyz)
    # i forgot that i wanted to test negative temperatures:
    z = [-zval for zval in z]



    # define x and y bin boxes
    xsteps = ysteps = 1
    bins = [np.arange(min(x) - xsteps/2., max(x) + xsteps/2. + xsteps, xsteps), np.arange(min(y) - ysteps/2., max(y) + ysteps/2.+ ysteps, ysteps)]

    # Define xlabels per xvalue
    xlabels = {xval: '3I' + str(xval) for xval in sorted(x)}
    # Define ylabels per yvalue
    ylabels = {yval: str(yval) + 'MN' for yval in sorted(y)}



    # make a figure
    fig = plt.figure(figsize=(12, 7))

    # plot histogram
    temphist, xedges, yedges, image = plt.hist2d(x, y, bins=bins, weights=z)
    #plt.legend()

    # Make a colorbar for the values in the 2d histogram
    cbar = fig.colorbar(image)
    plt.set_cmap('rainbow') # nicer colors

    # write values in boxes
    # This may need some tuning to center them
    for val in xyz:
        print val[2]
        if val[2] not in [np.nan, 'nan']:
            plt.annotate(-val[2], xy=(val[0], val[1]), xycoords='data', fontweight='bold', fontsize=20)
        else:
            plt.annotate('no\nreading', xy=(val[0], val[1]), xycoords='data', fontsize=18)

    # Set plot labels nice and large
    plt.xlabel('random x', fontsize=20)
    plt.ylabel('random y', fontsize=20)
    cbar.ax.tick_params(labelsize=20)
    plt.title('temperatures made up by jory 2017-07-31 15:30', fontsize=18)
    cbar.set_label('temperature in $\degree$ C', fontsize=20)

    # Set ticklabels nice and large
    plt.yticks([yval for yval in ylabels], [ylabels[yval] for ycal in ylabels], fontsize=20)
    plt.xticks([xval for xval in xlabels], [xlabels[xval] for xcal in xlabels], fontsize=20)

    # Save figure as png and pdf
    plt.savefig('forfeng.png', bbox_inches='tight')
    plt.savefig('forfeng.pdf', bbox_inches='tight')



