import cx_Oracle
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import sql3 as sql
import sys
###from numberOfROCs import numberOfRocs

if __name__ == "__main__":
            db = "temps.db"
            tb = "tempsrb"
            if len(sys.argv) > 1:
                starttime = sys.argv[1]
                #stoptime = datetime.datetime(starttime make starttime + 3h?
            if len(sys.argv) > 2:
                stoptime = sys.argv[2]
            else:
                print "usage e.g."
                starttime = "2018-07-20 20:00:45.421363"
                stoptime = "2018-07-23 16:40:45.421363"
                channel = "PixelBarrel_BmI_1I_L4D2MN"
                channel = "PixelBarrel_BmI*L4*"
                print "python tempsrb.py", "'" + starttime + "' '" + stoptime + "' '" + channel + "'"
                sys.exit()
            ### Opening connection
            connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_adg') # offline
            # connection = cx_Oracle.connect('cms_trk_r/1A3C5E7G:FIN@cms_omds_lb') # online
            cursor = connection.cursor()
            cursor.arraysize=50


            headers = ['part', 'alias', 'since', 'update_count', 'value_converted', 'change_date', 'dpname' ]
            headers = ['alias', 'dpe_name', 'dpid', 'ts', 'value_number']

            #print 'create table temperatures (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
            print starttime
            print stoptime
            channel = 'PixelBarrel_BmI_%L4%'
            channel = "PixelBarrel_BmI_1I_L4D2MN"
            if len(sys.argv) > 3:
                channel = sys.argv[3]
                channel = channel.replace('*', '%')
            print channel


            query = """
with maxsince as (select max(since) latest from cms_trk_dcs_pvss_cond.aliases where
    (alias like '%""" + str(channel) + """%'
    )
    group by alias),
    dpname as (select rtrim(dpe_name,'.') dp, alias from cms_trk_dcs_pvss_cond.aliases,maxsince
    where
    (alias like '%""" + str(channel) + """%'
    )
    and since = maxsince.latest),
ident as (select id from cms_trk_dcs_pvss_cond.dp_name2id, dpname
    where cms_trk_dcs_pvss_cond.dp_name2id.dpname = dpname.DP)
select dpname.alias,rb.dpe_name, rb.dpid, rb.ts, rb.value_number from cms_trk_dcs_pvss_cond.eventhistory_rb rb,ident,dpname
where rb.dpid = ident.id
and value_number is not null
and rb.ts > to_timestamp('""" + str(starttime) + """', 'RRRR-MM-DD HH24:MI:SS.FF')
and rb.ts < to_timestamp('""" + str(stoptime) + """', 'RRRR-MM-DD HH24:MI:SS.FF') --and rownum < 10
order by dpname.alias,rb.ts
"""
            ### Define query to get temperatures for Barrel Pixels
            print query
            cursor.execute(query)
            #{"the_start_time" : starttime, "the_end_time" : stoptime})
            row = cursor.fetchall()
#
            fileCurrents = "tempsrb.txt"
            fcur = open(fileCurrents, "w+")
            #print headers

            for i in xrange(len(row)):
                #print len(row)
                #print "====> ", "".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n"
                fcur.write("".join([str(row[i][j]) + "   " for j in range(len(row[i]))]) +  "\n")
                vals = row[i]
                #print vals

                values = {}
                for h in range(len(headers)):
                    if type(vals[h]) == str:
                        values[headers[h]] =  vals[h].strip('cms_trk_dcs_4:')
                    else:
                        values[headers[h]] =  vals[h]
                #print "writing to db", values

                sql.dict_to_db(values, db, tb)
                if values['value_number'] != None:
                    print values['value_number'], 'found at', datetime.datetime.isoformat(values['ts']), "for", values['dpid'], values['alias']

            fcur.close()


            connection.close()





