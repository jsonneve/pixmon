#!/usr/bin/env python
import os,sys,string,commands,datetime,time

class DCSmaps:
# Map between the DCS measurements and the PVSS identifier.

  def __init__(self):
  # Create a map and a reverse map between the DCS names and the PLC identifiers
  # in the database.

    # First start by getting the map between the PVSS identifiers and the DCS names
    # by making a SQL query in Oracle (unfortunately Cx_Oracle is not available on
    # these machines). This gets the mapping from 
    # TK_PLCS/Pixel/crate02/module04/channel_read_01 to 37845
    if os.path.isfile("oracle.tmp"):
      os.remove("oracle.tmp")
    if os.path.isfile("oracle.input"):
      os.remove("oracle.input")
    oracleCmd = open("oracle.input","w")
    oracleCmd.write("select dpname,id from dp_name2id where dpname like 'cms_trk_dcs_%Pixel%channel_read_%';\nexit\n")
    oracleCmd.close()
    rc,output = commands.getstatusoutput("sqlplus CMS_TRK_DCS_PVSS_COND_R/p5pvssCondDB@CMS_OMDS_LB < oracle.input > oracle.tmp")
    if rc!=0:
      sys.stdout.write("Problem running the first Oracle query - abort execution\n")
      for line in string.split(output,"\n"):
        sys.stdout.write("%s\n"%line)
      sys.exit(1) 
    os.remove("oracle.input")

    entry = ""
    number = -1
    i = 0
    revmap = {}
    for line in open("oracle.tmp","r").readlines():
      line = line[:-1]
      i += 1
      if string.find(line,"TK_PLCS/Pixel")==14:
        entry = line[14:]
        number = i
      if entry!="" and i==(number+1):
        number = int(line)
        revmap[entry] = number
        entry=""
        number = -1
    os.remove("oracle.tmp")

    # Then match the information from the Pixel_Phase1_Names_v7.csv (file provided
    # by Christian Barth on 28 March 2017) that contains the map between the DCS
    # names and the location of the temperature/humidity measurements on the half
    # cylinders. This gives the match between 
    # PixelEndcap BpI DCDC 3A and TK_PLCS/Pixel/crate02/module04/channel_read_01.
    self.dcsmap = {}
    for line in open("Pixel_Phase1_Names_v7.csv").readlines():
      line = line[:-1]
      tokens = string.split(line,",")
      if tokens[1].find("PixelEndcap")==0:
        if tokens[0] in revmap.keys():
          self.dcsmap[tokens[1]] = revmap[tokens[0]]

    return

  def getItem(self,item): 
    return self.dcsmap[item]
   
  def printMap(self):
    sys.stdout.write("Map between DCS names and PLC identifiers\n")
    for entry in self.dcsmap.keys():
      print entry,self.dcsmap[entry]
    return

  def getMap(self):
    return self.dcsmap

  def writeOracleQuery(self):
  # Create the SQL query to extract the most recent measurements from the conditions DB.
    oneWeekAgo = time.strftime("%d-%b-%y",time.localtime(time.time()-7*86400))
    if os.path.isfile("oracle.input"):
      os.remove("oracle.input")
    oracleCmd = open("oracle.input","w")
    for plcName in self.dcsmap.keys():
      dpId = self.dcsmap[plcName]
      if string.find(plcName,"PixelEndcap")==0:
        if string.find(plcName," Humidity")>0:
          # Do nothing, humidity measurements apparently are not stored in the database.
          pass
        elif string.find(plcName," Air")>0:
          # Go back to the beginning of time (FPIX installation)for temperature measurements, as these may be quite stable
          oracleCmd.write("select * from (select dpid,change_date,value_converted from tkplcreadsensor where dpid=%s and change_date>'10-Mar-17' order by change_date desc) where rownum=1 order by rownum;\n"%dpId)
        else:
          # Go back only one week for all other measurements.
          oracleCmd.write("select * from (select dpid,change_date,value_converted from tkplcreadsensor where dpid=%s and change_date>'%s' order by change_date desc) where rownum=1 order by rownum;\n"%(dpId,oneWeekAgo))
    oracleCmd.write("exit\n")
    oracleCmd.close()

    return
  
def getMeasurements(map,krbUser):
# Get all the entries from the conditions DB for the last three weeks and
# order them in time and print out only the most recent one. This query is
# likely to fail for the humidity and air measurements that may be stable
# and not updated even on a timescale of a few weeks. For those measurements
# go back to the beginning of time (i.e. FPIX Phase 1 installation).
  map.writeOracleQuery()
  if os.path.isfile("oracle.tmp"):
    os.remove("oracle.tmp")
  sys.stdout.write("\n")
  sys.stdout.write("Extracting DCS measurements from the conditions DB\n")
  rc,output = commands.getstatusoutput("sqlplus CMS_TRK_DCS_PVSS_COND_R/p5pvssCondDB@CMS_OMDS_LB < oracle.input > oracle.tmp")
  if rc!=0:
    sys.stdout.write("Problem running the first Oracle query - abort execution\n")
    for line in string.split(output,"\n"):
      sys.stdout.write("%s\n"%line)
    sys.exit(1) 
  # os.remove("oracle.input")
  
  # Now parse the Oracle output and get all the temperature and humidity measurements.
  measurementsDCS = {}
  id = 0
  date = ""
  value = 0
  count = 0
  active = 0
  oldestMeasurement = time.strptime('31-Dec-2099 23.59','%d-%b-%Y %H.%M')
  for line in open("oracle.tmp","r").readlines():
    line = line[:-1]
    if string.find(line,"VALUE_CONVERTED")==0:
      id = 0
      date = ""
      value = 0
      count = 0
      active = 1
    count += 1
    if active==1:
      if count==3:
        id = int(line)
      elif count==4:
        # Bloody gymnastics to convert the Oracle timestamp (in UTC) to the localtime.
        timeMeasurement = time.strptime(line[:15]+' '+line[29:31],'%d-%b-%y %I.%M %p')
        timeMeasurement = time.mktime(timeMeasurement)-time.altzone
        timeMeasurement = time.localtime(timeMeasurement)
      elif count==5:
        value = "%6.2f"%float(line)
        measurementsDCS[id] = "%s %s"%(value,time.strftime("PLC measurement taken on %d-%b-%y at %H:%M",timeMeasurement))
        if timeMeasurement<oldestMeasurement:
          oldestMeasurement = timeMeasurement
        active = 0
  os.remove("oracle.tmp")
  sys.stdout.write("Total number of DCS measurements available %s\n"%len(measurementsDCS))
  sys.stdout.write("%s\n"%time.strftime("Earliest measurement recorded at %H:%M on %d-%b-%Y (DCS measuremnt)",oldestMeasurement))
  
  # Now get the temperature measurements from the CCU (this requires executing
  # a program remotely on one of the FPIX nodes).
  sys.stdout.write("\n")
  #sys.stdout.write("Extracting temperature measurements from the CCUs read out by the tracker FEC\n")
  #rc, output = commands.getstatusoutput("ssh %s@srv-s2b18-31-01 'sudo -u pixelpilot -H bash /nfshome0/pixelpilot/TriDAS/pixel/BPixelTools/ccu/getTempFromCCU.sh'"%krbUser)
  if rc!=0:
    sys.stdout.write("Problem running the getTempFromCCU.ssh remotely on srv-s2b18-30-01 - abort execution\n")
    for line in string.split(output,"\n"):
      sys.stdout.write("%s\n"%line)
    sys.exit(1) 
  measurementsCCU = {}
  countMeasurements = 0
  for line in string.split(output,"\n"):
    if line[2:4]==" B" and line[11:13]=="0x":
      cylinder = line[3:6]
      if not cylinder in measurementsCCU.keys():
        measurementsCCU[cylinder] = {}
      type = line[26:32]
      if type!="----":
        measurementsCCU[cylinder][type] = "%s from CCU"%float(line[34:43])
        if string.find(line,"disconnected channel")>0:
          measurementsCCU[cylinder][type] += " (disconnected channel)"
        elif string.find(line,"unreliable channel")>0:
          measurementsCCU[cylinder][type] += " (unreliable channel)"
        countMeasurements += 1
  sys.stdout.write("Total number of CCU measurements available %s\n\n"%countMeasurements)
        
  # Now transfer the data to a buffer where we have the measurements ordered
  # by cooling line.
  coolingLines = {}
  for hc in ['BmO', 'BpO', 'BmI', 'BpI']:
    coolingLines[hc] = {}
    for disk in ['Inner1', 'Outer1', 'Inner2', 'Outer2', 'Inner3', 'Outer3']:
      coolingLines[hc][disk]={}
  
  # Ambient temperature and humidity measurements.
  for hc in [ 'BmO', 'BpO', 'BmI', 'BpI']:
    coolingLines[hc]['TAmbient'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Air'%hc)]
    # coolingLines[hc]['HAmbient'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Humidity'%hc)]
  
  # All the measurements for the half cylinders.
  
  for hc in [ 'BmO', 'BpO', 'BmI', 'BpI']:
    ccuAvail = 0
    if measurementsCCU.has_key(hc):
      ccuAvail = 1
    coolingLines[hc]['Inner1']['Inlet'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s 1I_SF'%hc)]
    coolingLines[hc]['Inner1']['DCDC3rdRowSupply'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3A'%hc)]
    coolingLines[hc]['Inner1']['Disk'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Disk 1I'%hc)]
    coolingLines[hc]['Inner1']['Module1'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 1A'%hc)]
    coolingLines[hc]['Inner1']['Module2'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 1C'%hc)]
    coolingLines[hc]['Inner1']['DCDC3rdRowReturn'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3D'%hc)]
    if ccuAvail==1:
      coolingLines[hc]['Inner1']['DCDC2ndRowSupply'] = measurementsCCU[hc]['DCDC2a'] 
      coolingLines[hc]['Inner1']['DCDC1stRowSupply'] = measurementsCCU[hc]['DCDC1a'] 
      coolingLines[hc]['Inner1']['SupplyBeforeDisk'] = measurementsCCU[hc]['SDI1  ']
      coolingLines[hc]['Inner1']['ReturnAfterDisk'] = measurementsCCU[hc]['RDI1  ']
      coolingLines[hc]['Inner1']['DCDC1stRowReturn'] = measurementsCCU[hc]['DCDC1d'] 
      coolingLines[hc]['Inner1']['DCDC2ndRowReturn'] = measurementsCCU[hc]['DCDC2d'] 
      coolingLines[hc]['Inner1']['Outlet'] = measurementsCCU[hc]['RFI1  ']
      coolingLines[hc]['Inner1']['Module3'] = measurementsCCU[hc]['P1Atop']
      coolingLines[hc]['Inner1']['Module4'] = measurementsCCU[hc]['P1Btop']
      coolingLines[hc]['Inner1']['Module5'] = measurementsCCU[hc]['P1Ctop']
      coolingLines[hc]['Inner1']['Module6'] = measurementsCCU[hc]['P1Dtop']
    else:
      coolingLines[hc]['Inner1']['DCDC2ndRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['DCDC1stRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['SupplyBeforeDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['ReturnAfterDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['DCDC1stRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['DCDC2ndRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['Outlet'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['Module3'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['Module4'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['Module5'] = 'CCU measurements not available'
      coolingLines[hc]['Inner1']['Module6'] = 'CCU measurements not available'
  
    coolingLines[hc]['Inner2']['Inlet'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s 2I_SF'%hc)]
    coolingLines[hc]['Inner2']['DCDC3rdRowSupply'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3B'%hc)]
    coolingLines[hc]['Inner2']['Disk'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Disk 2I'%hc)]
    coolingLines[hc]['Inner2']['Module1'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 2B'%hc)]
    coolingLines[hc]['Inner2']['Module2'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 2D'%hc)]
    coolingLines[hc]['Inner2']['DCDC3rdRowReturn'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3D'%hc)]
    if ccuAvail==1:
      coolingLines[hc]['Inner2']['DCDC2ndRowSupply'] = measurementsCCU[hc]['DCDC2b'] 
      coolingLines[hc]['Inner2']['DCDC1stRowSupply'] = measurementsCCU[hc]['DCDC1b'] 
      coolingLines[hc]['Inner2']['SupplyBeforeDisk'] = measurementsCCU[hc]['SDI2  ']
      coolingLines[hc]['Inner2']['ReturnAfterDisk'] = measurementsCCU[hc]['RDI2  ']
      coolingLines[hc]['Inner2']['DCDC1stRowReturn'] = measurementsCCU[hc]['DCDC1d'] 
      coolingLines[hc]['Inner2']['DCDC2ndRowReturn'] = measurementsCCU[hc]['DCDC2d'] 
      coolingLines[hc]['Inner2']['Outlet'] = measurementsCCU[hc]['RFI2  ']
      coolingLines[hc]['Inner2']['Module3'] = measurementsCCU[hc]['P2Atop']
      coolingLines[hc]['Inner2']['Module4'] = measurementsCCU[hc]['P2Btop']
      coolingLines[hc]['Inner2']['Module5'] = measurementsCCU[hc]['P2Ctop']
      coolingLines[hc]['Inner2']['Module6'] = measurementsCCU[hc]['P2Dtop']
    else:
      coolingLines[hc]['Inner2']['DCDC2ndRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['DCDC1stRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['SupplyBeforeDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['ReturnAfterDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['DCDC1stRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['DCDC2ndRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['Outlet'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['Module3'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['Module4'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['Module5'] = 'CCU measurements not available'
      coolingLines[hc]['Inner2']['Module6'] = 'CCU measurements not available'
  
    coolingLines[hc]['Inner3']['Inlet'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s 3I_SF'%hc)]
    coolingLines[hc]['Inner3']['DCDC3rdRowSupply'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3C'%hc)]
    coolingLines[hc]['Inner3']['Disk'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Disk 3I'%hc)]
    coolingLines[hc]['Inner3']['Module1'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 3A'%hc)]
    coolingLines[hc]['Inner3']['Module2'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 3C'%hc)]
    coolingLines[hc]['Inner3']['DCDC1stRowReturn'] = 'not connected to cooling line'
    coolingLines[hc]['Inner3']['DCDC2ndRowReturn'] = 'not connected to cooling line'
    coolingLines[hc]['Inner3']['DCDC3rdRowReturn'] = 'not connected to cooling line'
    if ccuAvail==1:
      coolingLines[hc]['Inner3']['DCDC2ndRowSupply'] = measurementsCCU[hc]['DCDC2c'] 
      coolingLines[hc]['Inner3']['DCDC1stRowSupply'] = measurementsCCU[hc]['DCDC1c'] 
      coolingLines[hc]['Inner3']['SupplyBeforeDisk'] = measurementsCCU[hc]['SDI3  ']
      coolingLines[hc]['Inner3']['ReturnAfterDisk'] = measurementsCCU[hc]['RDI3  ']
      coolingLines[hc]['Inner3']['Outlet'] = measurementsCCU[hc]['RFI3  ']
      coolingLines[hc]['Inner3']['Module3'] = measurementsCCU[hc]['P3Atop']
      coolingLines[hc]['Inner3']['Module4'] = measurementsCCU[hc]['P3Btop']
      coolingLines[hc]['Inner3']['Module5'] = measurementsCCU[hc]['P3Ctop']
      coolingLines[hc]['Inner3']['Module6'] = measurementsCCU[hc]['P3Dtop']
    else:
      coolingLines[hc]['Inner3']['DCDC2ndRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['DCDC1stRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['SupplyBeforeDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['ReturnAfterDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['DCDC1stRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['DCDC2ndRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['Outlet'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['Module3'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['Module4'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['Module5'] = 'CCU measurements not available'
      coolingLines[hc]['Inner3']['Module6'] = 'CCU measurements not available'
  
    coolingLines[hc]['Outer1']['Inlet'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s 1O_SF'%hc)]
    coolingLines[hc]['Outer1']['DCDC3rdRowSupply'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3D'%hc)]
    coolingLines[hc]['Outer1']['Disk'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Disk 1O'%hc)]
    coolingLines[hc]['Outer1']['Module1'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 1B'%hc)]
    coolingLines[hc]['Outer1']['Module2'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 1D'%hc)]
    coolingLines[hc]['Outer1']['DCDC3rdRowReturn'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3A'%hc)]
    if ccuAvail==1:
      coolingLines[hc]['Outer1']['DCDC2ndRowSupply'] = measurementsCCU[hc]['DCDC2d'] 
      coolingLines[hc]['Outer1']['DCDC1stRowSupply'] = measurementsCCU[hc]['DCDC1d'] 
      coolingLines[hc]['Outer1']['SupplyBeforeDisk'] = measurementsCCU[hc]['SDO1  ']
      coolingLines[hc]['Outer1']['ReturnAfterDisk'] = measurementsCCU[hc]['RDO1  ']
      coolingLines[hc]['Outer1']['DCDC1stRowReturn'] = measurementsCCU[hc]['DCDC1a'] 
      coolingLines[hc]['Outer1']['DCDC2ndRowReturn'] = measurementsCCU[hc]['DCDC2a'] 
      coolingLines[hc]['Outer1']['Outlet'] = measurementsCCU[hc]['RFO1  ']
      coolingLines[hc]['Outer1']['Module3'] = measurementsCCU[hc]['P1Abot']
      coolingLines[hc]['Outer1']['Module4'] = measurementsCCU[hc]['P1Bbot']
      coolingLines[hc]['Outer1']['Module5'] = measurementsCCU[hc]['P1Cbot']
      coolingLines[hc]['Outer1']['Module6'] = measurementsCCU[hc]['P1Dbot']
    else:
      coolingLines[hc]['Outer1']['DCDC2ndRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['DCDC1stRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['SupplyBeforeDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['ReturnAfterDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['DCDC1stRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['DCDC2ndRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['Outlet'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['Module3'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['Module4'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['Module5'] = 'CCU measurements not available'
      coolingLines[hc]['Outer1']['Module6'] = 'CCU measurements not available'
  
    coolingLines[hc]['Outer2']['Inlet'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s 2O_SF'%hc)]
    coolingLines[hc]['Outer2']['DCDC3rdRowSupply'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3C'%hc)]
    coolingLines[hc]['Outer2']['Disk'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Disk 2O'%hc)]
    coolingLines[hc]['Outer2']['Module1'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 2A'%hc)]
    coolingLines[hc]['Outer2']['Module2'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 2C'%hc)]
    coolingLines[hc]['Outer2']['DCDC3rdRowReturn'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3A'%hc)]
    if ccuAvail==1:
      coolingLines[hc]['Outer2']['DCDC2ndRowSupply'] = measurementsCCU[hc]['DCDC2c'] 
      coolingLines[hc]['Outer2']['DCDC1stRowSupply'] = measurementsCCU[hc]['DCDC1c'] 
      coolingLines[hc]['Outer2']['SupplyBeforeDisk'] = measurementsCCU[hc]['SDO2  ']
      coolingLines[hc]['Outer2']['ReturnAfterDisk'] = measurementsCCU[hc]['RDO2  ']
      coolingLines[hc]['Outer2']['DCDC1stRowReturn'] = measurementsCCU[hc]['DCDC1a'] 
      coolingLines[hc]['Outer2']['DCDC2ndRowReturn'] = measurementsCCU[hc]['DCDC2a'] 
      coolingLines[hc]['Outer2']['Outlet'] = measurementsCCU[hc]['RFO2  ']
      coolingLines[hc]['Outer2']['Module3'] = measurementsCCU[hc]['P2Abot']
      coolingLines[hc]['Outer2']['Module4'] = measurementsCCU[hc]['P2Bbot']
      coolingLines[hc]['Outer2']['Module5'] = measurementsCCU[hc]['P2Cbot']
      coolingLines[hc]['Outer2']['Module6'] = measurementsCCU[hc]['P2Dbot']
    else:
      coolingLines[hc]['Outer2']['DCDC2ndRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['DCDC1stRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['SupplyBeforeDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['ReturnAfterDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['DCDC1stRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['DCDC2ndRowReturn'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['Outlet'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['Module3'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['Module4'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['Module5'] = 'CCU measurements not available'
      coolingLines[hc]['Outer2']['Module6'] = 'CCU measurements not available'
    
    coolingLines[hc]['Outer3']['Inlet'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s 3O_SF'%hc)]
    coolingLines[hc]['Outer3']['DCDC3rdRowSupply'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s DCDC 3B'%hc)]
    coolingLines[hc]['Outer3']['Disk'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s Disk 3O'%hc)]
    coolingLines[hc]['Outer3']['Module1'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 3B'%hc)]
    coolingLines[hc]['Outer3']['Module2'] = measurementsDCS[mapDCS.getItem('PixelEndcap %s PC 3D'%hc)]
    coolingLines[hc]['Outer3']['DCDC1stRowReturn'] = 'not connected to cooling line'
    coolingLines[hc]['Outer3']['DCDC2ndRowReturn'] = 'not connected to cooling line'
    coolingLines[hc]['Outer3']['DCDC3rdRowReturn'] = 'not connected to cooling line'
    if ccuAvail==1:
      coolingLines[hc]['Outer3']['DCDC2ndRowSupply'] = measurementsCCU[hc]['DCDC2b'] 
      coolingLines[hc]['Outer3']['DCDC1stRowSupply'] = measurementsCCU[hc]['DCDC1b'] 
      coolingLines[hc]['Outer3']['SupplyBeforeDisk'] = measurementsCCU[hc]['SDO3  ']
      coolingLines[hc]['Outer3']['ReturnAfterDisk'] = measurementsCCU[hc]['RDO3  ']
      coolingLines[hc]['Outer3']['Outlet'] = measurementsCCU[hc]['RFO3  ']
      coolingLines[hc]['Outer3']['Module3'] = measurementsCCU[hc]['P3Abot']
      coolingLines[hc]['Outer3']['Module4'] = measurementsCCU[hc]['P3Bbot']
      coolingLines[hc]['Outer3']['Module5'] = measurementsCCU[hc]['P3Cbot']
      coolingLines[hc]['Outer3']['Module6'] = measurementsCCU[hc]['P3Dbot']
    else:
      coolingLines[hc]['Outer3']['DCDC2ndRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['DCDC1stRowSupply'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['SupplyBeforeDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['ReturnAfterDisk'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['Outlet'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['Module3'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['Module4'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['Module5'] = 'CCU measurements not available'
      coolingLines[hc]['Outer3']['Module6'] = 'CCU measurements not available'

  return coolingLines

if __name__=="__main__":

  # First make sure that you are on a computer with SQL and you
  # have a Kerberos ticket.
  progname = sys.argv[0]
  hostname = os.uname()[1]
  username = os.getenv('USER')
  hasSQL = os.path.isfile("/usr/bin/sqlplus")
  hasKRB = os.path.isfile("/usr/bin/klist")
  hasToken = 0
  krbUser = ""
  if hasKRB:
    rc,output = commands.getstatusoutput("/usr/bin/klist")
    if rc==0:
      for line in string.split(output,"\n"):
        if line[0:18]=="Default principal:":
          tokens = string.split(line,": ")
          idx = tokens[1].find("@CMS")
          krbUser = tokens[1][0:idx]
          hasToken = 1
  sys.stdout.write("Executing %s on %s from account %s\n"%(progname,hostname,username))
  if hasSQL==0:
    sys.stdout.write("You cannot run this program on this machine because SQLPLUS is not available\n")
    sys.stdout.write("Please run this program from one of the control room computers (SXC5SCR**)\n")
    sys.exit(1)
  if hasToken==0:
    sys.stdout.write("You cannot run this program without having a valid Kerberos ticket\n")
    sys.stdout.write("One option is to be logged on under your account (i.e. not use a pixel*** or a tracker*** account)\n")
    sys.stdout.write("The other option is to get a Kerberos ticket with kinit [username]\n")
    sys.exit(1)

  # Get the map between the names of the PLC sensors and the identifiers in the PLC.
  mapDCS = DCSmaps()
  coolingLines = getMeasurements(mapDCS,krbUser)
  
  # Plot the data on request from the user.
  userCommand = raw_input("Enter half cylinder/cooling line [ex BmO/Inner2] or Meas to make new measurements or blank to exit --> ")
  while userCommand!="":
    if string.upper(userCommand[0])=="M":
      coolingLines = getMeasurements(mapDCS,krbUser)
    elif string.upper(userCommand[0])=="Q":
      sys.exit(0)
    elif string.upper(userCommand[0])=='A':
      for halfCylinder in [ 'BmO', 'BmI', 'BpO', 'BpI']:
        for coolingLine in [ 'Inner1', 'Inner2', 'Inner3', 'Outer1', 'Outer2', 'Outer3' ]:
          sys.stdout.write("\n")
          sys.stdout.write("---------------------------------------------------------------------\n")
          sys.stdout.write("Temperature measurements for %s cooling line in half cylinder %s\n"%(coolingLine,halfCylinder))
          sys.stdout.write("---------------------------------------------------------------------\n")
          sys.stdout.write("Air temperature for %s: %s\n"%(halfCylinder,coolingLines[halfCylinder]['TAmbient']))
          for location in ['Inlet','DCDC3rdRowSupply','DCDC2ndRowSupply','DCDC1stRowSupply','SupplyBeforeDisk','Disk','Module1','Module2','Module3','Module4','Module5','Module6','ReturnAfterDisk','DCDC1stRowReturn','DCDC2ndRowReturn','DCDC3rdRowReturn','Outlet']:
            sys.stdout.write("Location %s Temperature (C) %s\n"%(location,coolingLines[halfCylinder][coolingLine][location]))
          sys.stdout.write("---------------------------------------------------------------------\n")
    else:
      tokens = string.split(userCommand,"/")
      halfCylinder = string.upper(tokens[0])
      coolingLine = string.upper(tokens[1])
      if halfCylinder=="BMO":
        halfCylinder = "BmO"
      if halfCylinder=="BPO":
        halfCylinder = "BpO"
      if halfCylinder=="BMI":
        halfCylinder = "BmI"
      if halfCylinder=="BPI":
        halfCylinder = "BpI"
      if coolingLine=="INNER1":
        coolingLine = "Inner1"
      if coolingLine=="INNER2":
        coolingLine = "Inner2"
      if coolingLine=="INNER3":
        coolingLine = "Inner3"
      if coolingLine=="OUTER1":
        coolingLine = "Outer1"
      if coolingLine=="OUTER2":
        coolingLine = "Outer2"
      if coolingLine=="OUTER3":
        coolingLine = "Outer3"
      sys.stdout.write("\n")
      sys.stdout.write("---------------------------------------------------------------------\n")
      sys.stdout.write("Temperature measurements for %s cooling line in half cylinder %s\n"%(coolingLine,halfCylinder))
      sys.stdout.write("---------------------------------------------------------------------\n")
      sys.stdout.write("Air temperature for %s: %s\n"%(halfCylinder,coolingLines[halfCylinder]['TAmbient']))
      for location in ['Inlet','DCDC3rdRowSupply','DCDC2ndRowSupply','DCDC1stRowSupply','SupplyBeforeDisk','Disk','Module1','Module2','Module3','Module4','Module5','Module6','ReturnAfterDisk','DCDC1stRowReturn','DCDC2ndRowReturn','DCDC3rdRowReturn','Outlet']:
        sys.stdout.write("Location %s Temperature (C) %s\n"%(location,coolingLines[halfCylinder][coolingLine][location]))
      sys.stdout.write("---------------------------------------------------------------------\n")
      sys.stdout.write("\n")
    userCommand = raw_input("Enter half cylinder/cooling line [ex BmO/Inner2] or Meas to make new measurements or blank to exit --> ")

