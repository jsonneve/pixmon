#!/usr/bin/env python
import sql3 as sql
import sys

db = "temps.db"
tb = "temperatures"
if __name__ == "__main__":
    if len(sys.argv) > 1:
        names = open(sys.argv[1], 'r')
    else:
        names = open('tempsall.txt', 'r')
    line = names.readline()
    headers = ['part', 'alias', 'since', 'update_count', 'value_converted', 'change_date', 'dpname' ]
    print 'create table temperatures (' + ''.join([h + ' text, ' for h in headers]) + 'unique(' + ''.join([h + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
    stop = 0
    maxlines = 3000
    maxlines = 13
    line = names.readline()
    while line != '': # and stop < maxlines:
        #vals = line.split(',')
        vals = line.split()
        vals_timestamps_correct = vals[:2]
        vals_timestamps_correct.append(vals[2] + ' ' + vals[3])
        vals_timestamps_correct += vals[4:6]
        vals_timestamps_correct.append(vals[6] + ' ' + vals[7])
        vals_timestamps_correct += vals[8:-1]
        vals_timestamps_correct.append(vals[-1][vals[-1].index(':') + 1:])

        values = {headers[i]: vals_timestamps_correct[i] for i in range(len(headers))}
        stop += 1
        sql.dict_to_db(values, db, tb)
        line = names.readline()