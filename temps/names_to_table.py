#!/usr/bin/env zsh
import sql3 as sql

db = "temps.db"
tb = "datapoint_names"
if __name__ == "__main__":
    names = open('Pixel_Phase1_Names_v7.csv', 'r')
    line = names.readline()
    headers = {i: line.split(',')[i].lower() for i in range(len(line.split(',')))}
    headers[13] = 'new_or_unchanged'
    headers[9] = 'sector'
    headers[6] = 'halfshell'
    for i in headers:
        if headers[i] == '':
            headers[i] = 'empty' + str(i)
    print 'create table datapoint_names (' + ''.join([headers[h] + ' text, ' for h in headers]) + 'unique(' + ''.join([headers[h] + ', ' for h in headers])[:-2] + ' ) on conflict replace);'
    stop = 0
    maxlines = 3000
    line = names.readline()
    while line != '': # and stop < maxlines:
        #vals = line.split(',')
        values = {headers[i]: line.split(',')[i].strip() for i in headers}
        stop += 1
        sql.dict_to_db(values, db, tb)
        line = names.readline()





